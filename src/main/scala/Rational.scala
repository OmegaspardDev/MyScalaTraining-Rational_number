import scala.annotation.tailrec

object Rational {
  def apply(numeratorArg: Int, denominatorArg: Int): Rational = new Rational(numeratorArg, denominatorArg)
  def apply(numeratorArg:Int) = new Rational(numeratorArg)
}

class Rational private (numeratorArg:Int, denominatorArg:Int) extends Ordered[Rational]{
  require(denominatorArg!=0)
  private val  numerator = numeratorArg/gcd(numeratorArg, denominatorArg)
  private val  denominator = denominatorArg/gcd(numeratorArg, denominatorArg)

  def this(numeratorArg:Int) {
    this(numeratorArg, 1)
  }

  private def toDecimal = numerator/denominator

  @tailrec
  private def gcd(num1:Int, num2:Int):Int = if(num2 == 0) num1 else gcd(num2, num1%num2)

  override def toString:String = numerator + " / " + denominator


  def +(that:Rational) = {
    new Rational(numerator * that.denominator + that.numerator * denominator, denominator * that.denominator)
  }

  def -(that:Rational) =
    new Rational(numerator * that.denominator - that.numerator * denominator, denominator * that.denominator)

  def *(that:Rational) =
    new Rational(numerator * that.numerator, denominator * that.denominator)

  def /(that:Rational) =
    new Rational(numerator * that.denominator, denominator * that.numerator)

  override def compare(that: Rational): Int = (this - that).toDecimal
}

object Main extends App {
  val r1 = Rational(4,8)
  println(r1)
}
